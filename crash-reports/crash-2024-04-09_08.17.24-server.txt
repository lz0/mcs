---- Minecraft Crash Report ----
// Daisy, daisy...

Time: 2024-04-09 08:17:24
Description: Exception in server tick loop

java.lang.NoClassDefFoundError: me/lucko/fabric/api/permissions/v0/Permissions
	at com.selfdot.cobblemontrainers.fabric.FabricPermissionValidator.hasPermission(FabricPermissionValidator.kt:11)
	at com.selfdot.cobblemontrainers.command.permission.CommandRequirementBuilder.hasPermission(CommandRequirementBuilder.java:33)
	at com.selfdot.cobblemontrainers.command.permission.CommandRequirementBuilder.lambda$build$1(CommandRequirementBuilder.java:40)
	at com.mojang.brigadier.tree.CommandNode.canUse(CommandNode.java:65)
	at net.minecraft.class_2170.method_9239(class_2170.java:346)
	at net.minecraft.class_2170.method_9239(class_2170.java:369)
	at net.minecraft.class_2170.method_9241(class_2170.java:340)
	at net.minecraft.class_3324.method_14596(class_3324.java:665)
	at net.minecraft.class_3324.method_14576(class_3324.java:566)
	at net.minecraft.class_3324.method_14570(class_3324.java:221)
	at net.minecraft.class_3248.method_33800(class_3248.java:131)
	at net.minecraft.class_3248.method_14384(class_3248.java:118)
	at net.minecraft.class_3248.redirect$zip000$fabric-networking-api-v1$handlePlayerJoin(class_3248.java:563)
	at net.minecraft.class_3248.method_18784(class_3248.java:68)
	at net.minecraft.class_2535.method_10754(class_2535.java:259)
	at net.minecraft.class_3242.method_14357(class_3242.java:172)
	at net.minecraft.server.MinecraftServer.method_3813(MinecraftServer.java:908)
	at net.minecraft.class_3176.method_3813(class_3176.java:283)
	at net.minecraft.server.MinecraftServer.method_3748(MinecraftServer.java:824)
	at net.minecraft.server.MinecraftServer.method_29741(MinecraftServer.java:671)
	at net.minecraft.server.MinecraftServer.method_29739(MinecraftServer.java:265)
	at java.base/java.lang.Thread.run(Thread.java:1583)
Caused by: java.lang.ClassNotFoundException: me.lucko.fabric.api.permissions.v0.Permissions
	at java.base/jdk.internal.loader.BuiltinClassLoader.loadClass(BuiltinClassLoader.java:641)
	at java.base/java.lang.ClassLoader.loadClass(ClassLoader.java:526)
	at net.fabricmc.loader.impl.launch.knot.KnotClassDelegate.loadClass(KnotClassDelegate.java:226)
	at net.fabricmc.loader.impl.launch.knot.KnotClassLoader.loadClass(KnotClassLoader.java:119)
	at java.base/java.lang.ClassLoader.loadClass(ClassLoader.java:526)
	... 22 more


A detailed walkthrough of the error, its code path and all known details is as follows:
---------------------------------------------------------------------------------------

-- System Details --
Details:
	Minecraft Version: 1.20.1
	Minecraft Version ID: 1.20.1
	Operating System: Linux (amd64) version 6.1.75+
	Java Version: 21.0.1, GraalVM Community
	Java VM Version: OpenJDK 64-Bit Server VM (mixed mode, sharing), GraalVM Community
	Memory: 854718152 bytes (815 MiB) / 2147483648 bytes (2048 MiB) up to 2147483648 bytes (2048 MiB)
	CPUs: 2
	Processor Vendor: AuthenticAMD
	Processor Name: AMD EPYC 7B12
	Identifier: AuthenticAMD Family 23 Model 49 Stepping 0
	Microarchitecture: Zen 2
	Frequency (GHz): -0.00
	Number of physical packages: 1
	Number of physical CPUs: 1
	Number of logical CPUs: 2
	Graphics card #0 name: unknown
	Graphics card #0 vendor: unknown
	Graphics card #0 VRAM (MB): 0.00
	Graphics card #0 deviceId: unknown
	Graphics card #0 versionInfo: unknown
	Virtual memory max (MB): 3975.48
	Virtual memory used (MB): 4587.82
	Swap memory total (MB): 0.00
	Swap memory used (MB): 0.00
	JVM Flags: 5 total; -XX:ThreadPriorityPolicy=1 -XX:+UnlockExperimentalVMOptions -XX:+EnableJVMCIProduct -XX:-UnlockExperimentalVMOptions -Xmx2G
	Fabric Mods: 
		adorn: Adorn 5.0.1+1.20.1
		architectury: Architectury 9.2.14
		beansbackpacks: BeansBackpacks 0.23-v2
		chunky: Chunky 1.3.138
		cobblemon: Cobblemon 1.4.1+1.20.1
		cobblemontrainers: CobblemonTrainers 0.9.12+1.20.1
		cobblemounts: CobbleMounts 1.2.1
			com_moandjiezana_toml_toml4j: toml4j 0.7.2
		dynamiclights: Dynamic Lights 1.7.1+mod
		fabric-api: Fabric API 0.92.0+1.20.1
			fabric-api-base: Fabric API Base 0.4.31+1802ada577
			fabric-api-lookup-api-v1: Fabric API Lookup API (v1) 1.6.36+1802ada577
			fabric-biome-api-v1: Fabric Biome API (v1) 13.0.13+1802ada577
			fabric-block-api-v1: Fabric Block API (v1) 1.0.11+1802ada577
			fabric-block-view-api-v2: Fabric BlockView API (v2) 1.0.1+1802ada577
			fabric-command-api-v1: Fabric Command API (v1) 1.2.34+f71b366f77
			fabric-command-api-v2: Fabric Command API (v2) 2.2.13+1802ada577
			fabric-commands-v0: Fabric Commands (v0) 0.2.51+df3654b377
			fabric-containers-v0: Fabric Containers (v0) 0.1.64+df3654b377
			fabric-content-registries-v0: Fabric Content Registries (v0) 4.0.11+1802ada577
			fabric-convention-tags-v1: Fabric Convention Tags 1.5.5+1802ada577
			fabric-crash-report-info-v1: Fabric Crash Report Info (v1) 0.2.19+1802ada577
			fabric-data-attachment-api-v1: Fabric Data Attachment API (v1) 1.0.0+de0fd6d177
			fabric-data-generation-api-v1: Fabric Data Generation API (v1) 12.3.4+1802ada577
			fabric-dimensions-v1: Fabric Dimensions API (v1) 2.1.54+1802ada577
			fabric-entity-events-v1: Fabric Entity Events (v1) 1.6.0+1c78457f77
			fabric-events-interaction-v0: Fabric Events Interaction (v0) 0.6.2+1802ada577
			fabric-events-lifecycle-v0: Fabric Events Lifecycle (v0) 0.2.63+df3654b377
			fabric-game-rule-api-v1: Fabric Game Rule API (v1) 1.0.40+1802ada577
			fabric-item-api-v1: Fabric Item API (v1) 2.1.28+1802ada577
			fabric-item-group-api-v1: Fabric Item Group API (v1) 4.0.12+1802ada577
			fabric-lifecycle-events-v1: Fabric Lifecycle Events (v1) 2.2.22+1802ada577
			fabric-loot-api-v2: Fabric Loot API (v2) 1.2.1+1802ada577
			fabric-loot-tables-v1: Fabric Loot Tables (v1) 1.1.45+9e7660c677
			fabric-message-api-v1: Fabric Message API (v1) 5.1.9+1802ada577
			fabric-mining-level-api-v1: Fabric Mining Level API (v1) 2.1.50+1802ada577
			fabric-networking-api-v1: Fabric Networking API (v1) 1.3.11+1802ada577
			fabric-networking-v0: Fabric Networking (v0) 0.3.51+df3654b377
			fabric-object-builder-api-v1: Fabric Object Builder API (v1) 11.1.3+1802ada577
			fabric-particles-v1: Fabric Particles (v1) 1.1.2+1802ada577
			fabric-recipe-api-v1: Fabric Recipe API (v1) 1.0.21+1802ada577
			fabric-registry-sync-v0: Fabric Registry Sync (v0) 2.3.3+1802ada577
			fabric-rendering-data-attachment-v1: Fabric Rendering Data Attachment (v1) 0.3.37+92a0d36777
			fabric-rendering-fluids-v1: Fabric Rendering Fluids (v1) 3.0.28+1802ada577
			fabric-resource-conditions-api-v1: Fabric Resource Conditions API (v1) 2.3.8+1802ada577
			fabric-resource-loader-v0: Fabric Resource Loader (v0) 0.11.10+1802ada577
			fabric-screen-handler-api-v1: Fabric Screen Handler API (v1) 1.3.30+1802ada577
			fabric-transfer-api-v1: Fabric Transfer API (v1) 3.3.4+1802ada577
			fabric-transitive-access-wideners-v1: Fabric Transitive Access Wideners (v1) 4.3.1+1802ada577
		fabric-language-kotlin: Fabric Language Kotlin 1.10.19+kotlin.1.9.23
			org_jetbrains_kotlin_kotlin-reflect: kotlin-reflect 1.9.23
			org_jetbrains_kotlin_kotlin-stdlib: kotlin-stdlib 1.9.23
			org_jetbrains_kotlin_kotlin-stdlib-jdk7: kotlin-stdlib-jdk7 1.9.23
			org_jetbrains_kotlin_kotlin-stdlib-jdk8: kotlin-stdlib-jdk8 1.9.23
			org_jetbrains_kotlinx_atomicfu-jvm: atomicfu-jvm 0.23.2
			org_jetbrains_kotlinx_kotlinx-coroutines-core-jvm: kotlinx-coroutines-core-jvm 1.8.0
			org_jetbrains_kotlinx_kotlinx-coroutines-jdk8: kotlinx-coroutines-jdk8 1.8.0
			org_jetbrains_kotlinx_kotlinx-datetime-jvm: kotlinx-datetime-jvm 0.5.0
			org_jetbrains_kotlinx_kotlinx-serialization-cbor-jvm: kotlinx-serialization-cbor-jvm 1.6.3
			org_jetbrains_kotlinx_kotlinx-serialization-core-jvm: kotlinx-serialization-core-jvm 1.6.3
			org_jetbrains_kotlinx_kotlinx-serialization-json-jvm: kotlinx-serialization-json-jvm 1.6.3
		fabricloader: Fabric Loader 0.15.7
			mixinextras: MixinExtras 0.3.5
		fallingtree: FallingTree 4.3.4
		ferritecore: FerriteCore 6.0.1
		fightorflight: Cobblemon Fight or Flight Fabric 0.5.0
			cloth-config: Cloth Config v11 11.1.106
				cloth-basic-math: cloth-basic-math 0.6.1
		java: OpenJDK 64-Bit Server VM 21
		letmedespawn: Let Me Despawn 1.2.0
		lithium: Lithium 0.11.2
		minecraft: Minecraft 1.20.1
		mr_veinminer: Veinminer 1.1.0
		noisium: Noisium 2.0.1+mc1.20.x
	Server Running: true
	Player Count: 0 / 1000; []
	Data Packs: vanilla, fabric
	Enabled Feature Flags: minecraft:vanilla
	World Generation: Stable
	Is Modded: Definitely; Server brand changed to 'fabric'
	Type: Dedicated Server (map_server.txt)